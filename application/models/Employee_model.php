<?php
class Employee_model extends CI_Model
{
	function findById($id)
	{
		$query = "SELECT * FROM employee WHERE employee_id = '$id'";
		$result = $this->db->query($query);
		return $result;
	}
	function findAll($limit = 10,$offset= 0){
		$query = "SELECT * FROM employee LIMIT $limit OFFSET $offset";
		$result = $this->db->query($query);	
		return $result;
	}
	function findAllWhere($limit = 10,$offset= 0,$where='1',$date1='',$date2=''){
		$where_date = "";
		if(!empty($date1))
			$date1 = date("Y-m-d", strtotime($date1));
		if(!empty($date2))
			$date2 = date("Y-m-d", strtotime($date2));
			
		if(!empty($date1) && empty($date2))
			$where_date = " AND employee_contractdate BETWEEN CAST('$date1' as DATE) AND CAST('$date1' as DATE) ";
		if(!empty($date1) && !empty($date2))
			$where_date = " AND employee_contractdate BETWEEN CAST('$date1' as DATE) AND CAST('$date2' as DATE) ";
		if(empty($date1) && !empty($date2))
			$where_date = " AND employee_contractdate BETWEEN CAST('$date2' as DATE) AND CAST('$date2' as DATE) ";
		if(empty($date1) && empty($date2))
			$where_date = "";
			
		if($where == '1')
			$query = "SELECT * FROM employee LIMIT $limit OFFSET $offset";
		else
			$query = "SELECT * FROM employee 
			WHERE (employee_id LIKE '%$where%' OR 
			employee_name LIKE '%$where%' OR
			employee_email LIKE '%$where%' OR
			employee_address LIKE '%$where%')
			$where_date
			LIMIT $limit OFFSET $offset";
		$result = $this->db->query($query);	
		return $result;
	}	
	function findAllOrderBy($limit = 10,$offset= 0,$where='1',$orderby='',$date1='',$date2='',$order='ASC'){
		if($this->session->has_userdata('orderby')){
			$last_orderby = $this->session->userdata('orderby');
			$last_order = $this->session->userdata('order');
			if($last_orderby == $orderby)
			{
				if($last_order == 'ASC')
					$order = 'DESC';
				else
					$order = 'ASC';
			}
		}
		$where_date = "";
		if(!empty($date1))
			$date1 = date("Y-m-d", strtotime($date1));
		if(!empty($date2))
			$date2 = date("Y-m-d", strtotime($date2));
			
		if(!empty($date1) && empty($date2))
			$where_date = " AND employee_contractdate BETWEEN CAST('$date1' as DATE) AND CAST('$date1' as DATE) ";
		if(!empty($date1) && !empty($date2))
			$where_date = " AND employee_contractdate BETWEEN CAST('$date1' as DATE) AND CAST('$date2' as DATE) ";
		if(empty($date1) && !empty($date2))
			$where_date = " AND employee_contractdate BETWEEN CAST('$date2' as DATE) AND CAST('$date2' as DATE) ";
		if(empty($date1) && empty($date2))
			$where_date = "";
		
		if($where == '1')
			$query = "SELECT * FROM employee ORDER BY $orderby $order LIMIT $limit OFFSET $offset";
		else
			$query = "SELECT * FROM employee 
			WHERE (employee_id LIKE '%$where%' OR 
			employee_name LIKE '%$where%' OR
			employee_email LIKE '%$where%' OR
			employee_address LIKE '%$where%') 
			$where_date 
			ORDER BY $orderby $order
			LIMIT $limit OFFSET $offset";
		echo $query;
		$result = $this->db->query($query);	
		$order = array(
			'orderby' => $orderby,
			'order' => $order
		);
		$this->session->set_userdata($order);
		return $result;
	}
	function register($id,$name,$addr,$email,$tel,$c_date,$bday,$f,$hf){
		$c_date = str_replace("/","-",$c_date);
		$c_date = date('Y-m-d',strtotime($c_date));
		$bday = str_replace("/","-",$bday);
		$bday = date('Y-m-d',strtotime($bday));
		$data = array(
			'employee_id'=>$id,
			'employee_name'=>$name,
			'employee_address'=>$addr,
			'employee_email'=>$email,
			'employee_phonenumber'=>$tel,
			'employee_contractdate'=>$c_date,
			'employee_birthdate'=>$bday,
			'employee_freelance'=>$f,
			'employee_hourlyfee'=>$hf
		);		

		try{
			$this->db->insert('employee',$data);
		}catch(Exception $e){
			return 0;
		};
		return $this->db->affected_rows(); 
	}
	function update($id,$name,$addr,$email,$tel,$c_date,$bday,$f,$hf){
		$c_date = str_replace("/","-",$c_date);
		$c_date = date('Y-m-d',strtotime($c_date));
		$bday = str_replace("/","-",$bday);
		$bday = date('Y-m-d',strtotime($bday));
		$data = array(
			'employee_id'=>$id,
			'employee_name'=>$name,
			'employee_address'=>$addr,
			'employee_email'=>$email,
			'employee_phonenumber'=>$tel,
			'employee_contractdate'=>$c_date,
			'employee_birthdate'=>$bday,
			'employee_freelance'=>$f,
			'employee_hourlyfee'=>$hf
		);		
		try{
			$this->db->where('employee_id',$id);
			$this->db->update('employee',$data);
		}catch(Exception $e){
			return 0;
		};
		return $this->db->affected_rows(); 
	}	
	function check_id($id){
		$query = "SELECT * FROM Employee WHERE employee_id = '$id'";
		$res = $this->db->query($query);
		if($res->num_rows() == 0)
		{
			echo "-1";
		}else{
			echo "1";
		}
	}
	function delete($id){
		$employee_id = $id;
		$this->db->where('employee_id',$employee_id);
		$this->db->delete('employee');
		echo $this->db->affected_rows();
	}
	function countAll(){
		$query = "SELECT COUNT(*) as number FROM employee WHERE 1";
		$res = $this->db->query($query);
		return intval($res->row()->number);
	}
}
?>