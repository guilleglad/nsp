<?php
class User extends CI_Controller{
	
	function login(){ //AJAX
		//LOAD MODEL
		$this->load->model('User_model');
		//GET CREDENTIALS
		$name = $_POST["name"];
		$pwd = $_POST["pwd"];
		$remember = '0';
		if(isset($_POST["remember"]))
			$remember = $_POST["remember"];
		//CHECK SESSION DATA
		if(!$this->User_model->logged_in()){
			$res = $this->User_model->find($name,$pwd);	//CHECK USER
			if($res->num_rows() == 0){
				echo "Username or Password Invalid, please try again";
			}else{
				//USER FOUND
				$row = $res->row();
				//SET SESSION DATA
				$session_data = array(
					'username' => $row->admin_id,
					'fullname' => $row->admin_fullname,
					'remember' => $remember
				);
				$this->session->set_userdata($session_data);
				echo "OK";
			}			
		}

	}
	function logout(){ //AJAX
		$this->session->sess_destroy();
	}
	function logout2(){ //AJAX
		if($this->session->userdata('remember') == '0')
			$this->logout();
	}	

	function signup(){
		$this->load->model('User_model');			
		$logged_in = $this->User_model->logged_in();
		$this->load->view('user/head');
		$data = array('logged'=> $logged_in);
		$this->load->view('user/nav',$data);
		if($logged_in)
			$data = array('name'=>$this->session->userdata('fullname'));
		else
			$data = array('name'=>'');
		$this->load->view('user/header',$data);
		$this->load->view('user/content');
		$this->load->view('user/footer');

	}
	function register(){
		//LOAD MODEL
		$this->load->model('User_model');
		$username = $_POST['user_fullname'];
		$pwd = $_POST['user_password'];
		$id = $_POST['user_login'];
		$res = $this->User_model->register($username,$pwd,$id);
		$this->load->view('user/head');
		$logged_in = $this->User_model->logged_in();
		$data = array('logged'=> $logged_in);
		$this->load->view('user/nav',$data);
		if($logged_in)
			$data = array('name'=>$this->session->userdata('fullname'));
		else
			$data = array('name'=>'');
		$this->load->view('user/header',$data);		
		if($res == 1)
			$data = array('msg'=>"User Registered Succesfully",'res'=>$res);
		else
			$data = array('msg'=>"Ups! something happened! User was not Registered",'res'=>$res);
		$this->load->view('user/after_register',$data);
	}
}

?>