<?php
/**
* 
*/
class Main extends CI_Controller
{
	/*
	MAIN PAGE
	*/
	function index()
	{
		//LOAD MODEL
		$this->load->model('User_model');			
		//CHECK SESSION STATUS
		$logged_in = $this->User_model->logged_in();
		//LOAD HEAD
		$this->load->view('main/head');
		//LOAD NAV
		$data = array('logged'=> $logged_in);
		$this->load->view('main/nav',$data);
		if($logged_in)
			$data = array('name'=>$this->session->userdata('fullname'));
		else
			$data = array('name'=>'');
		//LOAD HEADER
		$this->load->view('main/header',$data);
		//LOAD CONTENT
		$this->load->view('main/content');
		//LOAD FOOTER
		$this->load->view('main/footer');

	}
}
?>