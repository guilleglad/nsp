<?php
class Employee extends CI_Controller{
	function index(){
		//LOAD MODELS
		$this->load->model('User_model');			
		$this->load->model('Employee_model');
		//CHECK SESSION STATE
		$logged_in = $this->User_model->logged_in();
		//LOAD HEAD
		$this->load->view('employee/head');
		//LOAD NAV
		$data = array('logged'=> $logged_in);
		$this->load->view('employee/nav',$data);
		//LOAD HEADER
		if($logged_in)
			$data = array('name'=>$this->session->userdata('fullname'));
		else
			$data = array('name'=>'');
		$this->load->view('employee/header',$data);
		//LOAD CONTENT
		$result = $this->Employee_model->findAll();
		$data = array(
			'data'=>$result->result(),
			'limit'=>'10',
			'offset'=>'0',
			'num_rows'=>$result->num_rows(),
			'total_numrows'=>$this->Employee_model->CountAll(),
			);

		$this->load->view('employee/content',$data);
		//LOAD FOOTER
		$this->load->view('employee/footer');
	}
	
	function index_ajax(){
		$limit = $_POST['limit'];
		$offset = $_POST['offset'];
		$html_res = "";
		$this->load->model('Employee_model');
		$result = $this->Employee_model->findAll($limit,$offset);
		$pos = $offset +1;
		foreach($result->result() as $row)
		{
		  $html_res .= '<tr>
   		    <td><span class="badge">'.$pos++.'</span></td>
			<td><div class="text-center">
			<a href="'.base_url().'employee/edit/'.$row->employee_id.'"><span class="glyphicon glyphicon-pencil"></span></a>
			<a href="'.base_url().'employee/delete/'.$row->employee_id.'" class="mydelete"><span class="glyphicon glyphicon-trash"></span></a>
			</div></td>
			<td>'.$row->employee_id.'</td>
			<td>'.$row->employee_name.'</td>
			<td>'.$row->employee_email.'</td>
		  </tr>';
		}
		$html_res .= '<input type="hidden" id="field_pos" value="'.$pos--.'"/>';		
		$html_res .= '<input type="hidden" id="total_numrows" value="'.$this->Employee_model->countAll().'"/>';		
		echo $html_res;
	}
	function index_ajax_where(){
		$limit = $_POST['limit'];
		$offset = $_POST['offset'];
		$where = $_POST['where'];
		$date1 = $_POST['date1'];
		$date2 = $_POST['date2'];
		$html_res = "";
		$this->load->model('Employee_model');
		$result = $this->Employee_model->findAllWhere($limit,$offset,$where,$date1,$date2);
		$pos = $offset +1;
		foreach($result->result() as $row)
		{
		  $html_res .= '<tr>
   		    <td><span class="badge">'.$pos++.'</span></td>
			<td><div class="text-center">
			<a href="'.base_url().'employee/edit/'.$row->employee_id.'"><span class="glyphicon glyphicon-pencil"></span></a>
			<a href="'.base_url().'employee/delete/'.$row->employee_id.'" class="mydelete"><span class="glyphicon glyphicon-trash"></span></a>
			</div></td>
			<td>'.$row->employee_id.'</td>
			<td>'.$row->employee_name.'</td>
			<td>'.$row->employee_email.'</td>
		  </tr>';
		}
		$html_res .= '<input type="hidden" id="field_pos" value="'.$pos--.'"/>';
		$html_res .= '<input type="hidden" id="total_numrows" value="'.$this->Employee_model->countAll().'"/>';
		echo $html_res;
	}	
	
	function index_orderby()
	{
		$limit = $_POST['limit'];
		$offset = $_POST['offset'];
		$orderby = $_POST['orderby'];
		$where = $_POST['where'];
		$date1 = $_POST['date1'];
		$date2 = $_POST['date2'];
		$html_res = "";
		$this->load->model('Employee_model');
		$result = $this->Employee_model->findAllOrderBy($limit,$offset,$where,$orderby,$date1,$date2);
		$pos = $offset +1;
		foreach($result->result() as $row)
		{
		  $html_res .= '<tr>
   		    <td><span class="badge">'.$pos++.'</span></td>
			<td><div class="text-center">
			<a href="'.base_url().'employee/edit/'.$row->employee_id.'"><span class="glyphicon glyphicon-pencil"></span></a>
			<a href="'.base_url().'employee/delete/'.$row->employee_id.'" class="mydelete"><span class="glyphicon glyphicon-trash"></span></a>
			</div></td>
			<td>'.$row->employee_id.'</td>
			<td>'.$row->employee_name.'</td>
			<td>'.$row->employee_email.'</td>
		  </tr>';
		}
		$html_res .= '<input type="hidden" id="field_pos" value="'.$pos--.'"/>';		
		$html_res .= '<input type="hidden" id="total_numrows" value="'.$this->Employee_model->countAll().'"/>';		
		echo $html_res;		
	}
	function create()
	{
		//LOAD MODELS
		$this->load->model('User_model');			
		$this->load->model('Employee_model');
		//CHECK SESSION STATE
		$logged_in = $this->User_model->logged_in();
		//LOAD HEAD
		$this->load->view('employee/head');
		//LOAD NAV
		$data = array('logged'=> $logged_in);
		$this->load->view('employee/nav',$data);
		//LOAD HEADER
		if($logged_in)
			$data = array('name'=>$this->session->userdata('fullname'));
		else
			$data = array('name'=>'');
		$this->load->view('employee/header_create',$data);
		//LOAD CONTENT
		$result = $this->Employee_model->findAll();
		$data = array(
			'data'=>$result->result(),
			'limit'=>'10',
			'offset'=>'0',
			'num_rows'=>$result->num_rows()
			);

		$this->load->view('employee/create',$data);
		//LOAD FOOTER
		$this->load->view('employee/footer_create');	
	}
	function register()
	{
		$this->load->model('Employee_model');
		
		$employee_id = $_POST['employee_id'];
		$employee_name = $_POST['employee_name'];
		$employee_address = $_POST['employee_address'];
		$employee_email = $_POST['employee_email'];
		$employee_phonenumber = $_POST['employee_phonenumber'];
		$employee_contractdate = $_POST['employee_contractdate'];
		$employee_birthday = $_POST['employee_birthday'];
		if(isset($_POST['employee_freelancer']))
			$employee_freelancer = $_POST['employee_freelancer'];
		else
			$employee_freelancer = '0';
		$employee_hourlyfee = $_POST['employee_hourlyfee'];

		$res = $this->Employee_model->register($employee_id,$employee_name,$employee_address,$employee_email,$employee_phonenumber,$employee_contractdate,$employee_birthday,$employee_freelancer,$employee_hourlyfee);
		
		//LOAD MODELS
		$this->load->model('User_model');			
		$this->load->model('Employee_model');
		//CHECK SESSION STATE
		$logged_in = $this->User_model->logged_in();
		//LOAD HEAD
		$this->load->view('employee/head');
		//LOAD NAV
		$data = array('logged'=> $logged_in);
		$this->load->view('employee/nav',$data);
		//LOAD HEADER
		$this->load->view('employee/header');
		
		$msg = '';
		if($res == '1')
		{
			$msg = 'New Record saved Succesfully';
		}else{
			$msg = 'Ups! Something happened, the record couldn\'t be saved.';
		}
		$data = array('msg'=>$msg,'res'=>$res);
		$this->load->view('employee/after_register',$data);		
	}
	function error(){
		
		//LOAD MODELS
		$this->load->model('User_model');			
		$this->load->model('Employee_model');
		//CHECK SESSION STATE
		$logged_in = $this->User_model->logged_in();
		//LOAD HEAD
		$this->load->view('employee/head');
		//LOAD NAV
		$data = array('logged'=> $logged_in);
		$this->load->view('employee/nav',$data);
		//LOAD HEADER
		if($logged_in)
			$data = array('name'=>$this->session->userdata('fullname'));
		else
			$data = array('name'=>'');
		$this->load->view('employee/header_create',$data);
		//LOAD CONTENT
		$result = $this->Employee_model->findAll();
		$data = array(
			'data'=>$result->result(),
			'limit'=>'10',
			'offset'=>'0',
			'num_rows'=>$result->num_rows()
			);

		$this->load->view('employee/error',$data);
		//LOAD FOOTER
		$this->load->view('employee/footer_create');	
	}
	function check_id(){
		$id = $_POST['employee_id'];
		$this->load->model('Employee_model');
		return $this->Employee_model->check_id($id);
		
	}
	function edit($id){
		$this->load->model('Employee_model');
		$res = $this->Employee_model->findById($id);
		$employee = $res->row();
		
		$employee_id = $employee->employee_id;
		$employee_name = $employee->employee_name;
		$employee_address = $employee->employee_address;
		$employee_email = $employee->employee_email;
		$employee_phonenumber = $employee->employee_phonenumber;
		$employee_contractdate = $employee->employee_contractdate;
		$employee_contractdate = date("d/m/Y",strtotime($employee_contractdate));		
		$employee_contractdate = str_replace("-","/",$employee_contractdate);
		$employee_birthday = $employee->employee_birthdate;
		$employee_birthday = date("d/m/Y",strtotime($employee_birthday));
		$employee_birthday = str_replace("-","/",$employee_birthday);
		$employee_freelancer = $employee->employee_freelance;
		$employee_hourlyfee = $employee->employee_hourlyfee;

	
		//LOAD MODELS
		$this->load->model('User_model');			
		$this->load->model('Employee_model');
		//CHECK SESSION STATE
		$logged_in = $this->User_model->logged_in();
		//LOAD HEAD
		$this->load->view('employee/head');
		//LOAD NAV
		$data = array('logged'=> $logged_in);
		$this->load->view('employee/nav_edit',$data);
		//LOAD HEADER
		$this->load->view('employee/header');
		
		$msg = '';
		$data = array(
		'employee_id'=>$employee_id,
		'employee_name'=>$employee_name,
		'employee_address'=>$employee_address,
		'employee_email'=>$employee_email,
		'employee_phonenumber'=>$employee_phonenumber,
		'employee_contractdate'=>$employee_contractdate,
		'employee_birthday'=>$employee_birthday,
		'employee_freelancer'=>$employee_freelancer,
		'employee_hourlyfee'=>$employee_hourlyfee
		);
		$this->load->view('employee/edit',$data);
		//LOAD FOOTER
		$this->load->view('employee/footer_edit');			
	}
	function update(){
		$this->load->model('Employee_model');

		$employee_id = $_POST['employee_id'];
		$employee_name = $_POST['employee_name'];
		$employee_address = $_POST['employee_address'];
		$employee_email = $_POST['employee_email'];
		$employee_phonenumber = $_POST['employee_phonenumber'];
		$employee_contractdate = $_POST['employee_contractdate'];
		$employee_birthday = $_POST['employee_birthday'];
		if(isset($_POST['employee_freelancer']))
			$employee_freelancer = $_POST['employee_freelancer'];
		else
			$employee_freelancer = '0';
		$employee_hourlyfee = $_POST['employee_hourlyfee'];

		$res = $this->Employee_model->update($employee_id,$employee_name,$employee_address,$employee_email,$employee_phonenumber,$employee_contractdate,$employee_birthday,$employee_freelancer,$employee_hourlyfee);
	
		//LOAD MODELS
		$this->load->model('User_model');			
		$this->load->model('Employee_model');
		//CHECK SESSION STATE
		$logged_in = $this->User_model->logged_in();
		//LOAD HEAD
		$this->load->view('employee/head');
		//LOAD NAV
		$data = array('logged'=> $logged_in);
		$this->load->view('employee/nav',$data);
		//LOAD HEADER
		$this->load->view('employee/header');
		
		$msg = '';
		if($res == '1')
		{
			$msg = 'Record updated Succesfully';
		}else{
			$msg = 'Ups! Something happened, the record couldn\'t be updated.';
		}
		$data = array('msg'=>$msg,'res'=>$res);
		$this->load->view('employee/after_update',$data);	
	}	
	function delete($id)
	{
		$employee_id = $id;
		//LOAD MODEL
		$this->load->model('Employee_model');
		$res = $this->Employee_model->delete($employee_id);
		$msg = '';
		if($res == '1')
		{
			$msg = 'Record updated Succesfully';
		}else{
			$msg = 'Ups! Something happened, the record couldn\'t be updated.';
		}
		return $msg;
	}
}