<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Login Form</h4>
      </div>
      <div class="modal-body">
        <!-- Main Content -->
        <div class="container" id="login_box">
            <div class="row">
                <div class="col-lg-6">
                    <div class="well well-sm text-center">
                        <form name="form_login" id="form_login" class="form-signin" action="<?= base_url(); ?>user/login" method="post" enctype="application/x-www-form-urlencoded">
                            <h2 class="form-signin-heading">Please sign in</h2>
                            <label for="inputUsername" class="sr-only">Username</label>
                            <input name="name" type="text" id="name" class="form-control" placeholder="UserName" required autofocus>
                            <label for="inputPassword" class="sr-only">Password</label>
                            <input name="pwd" type="password" id="pwd" class="form-control" placeholder="Password" required>
                            <div class="checkbox">
                              <label>
                                <input name="remember" id="remember" type="checkbox" value="1"> Remember me
                              </label>
                            </div>
                            <button class="btn btn-sm btn-primary btn-block" type="submit">Sign in</button>
                            <button class="btn btn-sm btn-info btn-block" type="button" id="signup_btn">Sign up</button>
                    	</form>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
      

