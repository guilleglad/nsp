<style>
.error{
	font-size:0.7em;
}

</style>
   <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <p>
                <label class="label label-info">Please Fill up all the Required Fields</label>
                <img id="loader" src="<?= base_url() ?>resources/img/loader.gif" class="hidden" width="24" height="24">
                </p>
   
			<form class="form-horizontal" role="form" id='registerform' action="<?= base_url(); ?>employee/register" method="post">
                <div class="form-group">
                    <label class="control-label col-sm-2 label label-info" for="employee_id">ID</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="employee_id" name="employee_id"
                            placeholder="Enter User ID" minlength="3" maxlength="10" required value="V-">
                        <span id="msg_id_used" class="label label-danger error" style="display:none;">ID Already Used</span>
                    </div>
                </div>            
                <div class="form-group">
                    <label class="control-label col-sm-2 label label-info" for="employee_name">Fullname</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="employee_name" name="employee_name"
                            placeholder="Enter Fullname" maxlength="100" minlength="10" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2 label label-info" for="employee_address">Address</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="employee_address" name="employee_address"
                            placeholder="Enter your Address"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2 label label-info" for="employee_email">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="employee_email" name="employee_email"
                            placeholder="Enter Email">
                    </div>
                </div>                
                <div class="form-group">
                    <label class="control-label col-sm-2 label label-info" for="employee_phonenumber">Phone Number</label>
                    <div class="col-sm-10">
                        <input type="tel" class="form-control" id="employee_phonenumber" name="employee_phonenumber"
                            placeholder="Enter your Phone Number">
                    </div>
                </div>                
                <div class="form-group">
                    <label class="control-label col-sm-2 label label-info" for="employee_contractdate">Contract Date</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="employee_contractdate" name="employee_contractdate"
                            placeholder="Enter your Contract Date">
                    </div>
                </div>               
                <div class="form-group">
                    <label class="control-label col-sm-2 label label-info" for="employee_birthday">Birthday</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="employee_birthday" name="employee_birthday"
                            placeholder="Enter your Birthday">
                    </div>
                </div>                     
                <div class="form-group">
                    <label class="control-label col-sm-2 label label-info" for="employee_freelancer">Freelancer?</label>
                    <div class="col-sm-10">
                    <div class="checkbox">
                      <label class="checkbox-inline">
                      	<input type="checkbox" value="1" id="employee_freelancer" name="employee_freelancer">Do you work as Freelancer?
                      </label>
                     </div>
                    </div>

                </div>                
                <div class="form-group" id="div_employee_hourlyfee">
                    <label class="control-label col-sm-2 label label-info" for="employee_hourlyfee">Hourly Fee</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="employee_hourlyfee" name="employee_hourlyfee"
                            placeholder="Enter your Hourly Fee">
                    </div>
                </div>                                                                                
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10 text-right">
                        <button type="submit" class="btn btn-default">Submit</button>
                        <button type="button" class="btn btn-default" id="go_back_employee">Go Back</button>
                    </div>                    
                </div>
            </form>
            </div>
        </div>
    </div>

    <hr>