<style>
.loader{
	display:none;
}
</style>
<div class="container small">
	<div class="row " id="row_show_filter" style="cursor:pointer;">
	    <div class="col-sm-12">
    		<a href="#"><span class="glyphicon glyphicon-filter"></span>Show Filters</a>
        </div>
    </div>
      <div class="row well well-sm" id="filter_row">
      	  <div class="col-sm-12">
          <a href="#">
              <div class="text-right">
              	<span class="glyphicon glyphicon-remove" id="filter_close_btn"></span>
              </div>
          </a>
          </div>
          <div class="col-sm-12">
          <h1>Filters</h1>
          </div>
          <div class="col-sm-4">
			<input class="form-control" type="text" id="mix_filter" placeholder="Mixed Filter for ID, Name, Email and Address"/>
          </div>
          <div class="col-sm-3">
                <input class="form-control" type="date" id="date1" placeholder="Contract Initial Date"/>
          </div>
          <div class="col-sm-3">
                <input class="form-control" type="date" id="date2" placeholder="Contract Final Date"/>
    	  </div>
	      <div class="col-sm-2">
        	  <div class="text-center"><button type="button" class="btn btn-primary" id="filter_btn">Apply Filter</button>
          </div>          
      </div>
</div>
<div class="row">
<div class="panel panel-primary">
      <div class="panel-heading"> <h2>EMPLOYEES </h2>
      <div class="text-right">
      <a href="<?= base_url() ?>employee/create"><button class="btn btn-info" id="btn_create_employee"> Create </button></a>
      </div></div>
      <div class="panel-body">
            <div class="table-responsive">          
              <table class="table">
                <thead>
                  <tr>
                    <th></th>
                    <th><div class="loader text-center">
                    <img src="<?= base_url(); ?>resources/img/loader.gif" width='24' height='24'/></div>
                    </th>
                    <th><div style="cursor:pointer;" class="" id="order_id" data-value="employee_id"> User ID <span id="id_chevron" class="glyphicon glyphicon-chevron-up hidden chevron_header"></span></div></th>
                    <th><div style="cursor:pointer;" class="" id="order_name" data-value="employee_name"> FullName <span id="name_chevron" class="glyphicon glyphicon-chevron-up hidden chevron_header"></span></div></th>
                    <th><div style="cursor:pointer;" class="" id="order_email" data-value="employee_email"> Email <span id="email_chevron" class="glyphicon glyphicon-chevron-up hidden chevron_header"></span></div></th>
                  </tr>
                </thead>
                <tbody id="table_body">
                <?php 
				$pos = $offset +1 ;
				foreach($data as $myRow){ 
				?>
                  <tr>
                    <td><span class="badge"><?=$pos++?></span></td>
                    <td><div class="text-center">
                    <a href="<?= base_url() ?>employee/edit/<?= $myRow->employee_id ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="<?= base_url() ?>employee/delete/<?= $myRow->employee_id ?>" class="mydelete"><span class="glyphicon glyphicon-trash"></span></a>
                    </div></td>
                    <td><?= $myRow->employee_id ?></td>
                    <td><?= $myRow->employee_name ?></td>
                    <td><?= $myRow->employee_email ?></td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
				<br>
              <div class="col-lg-12 text-center">
                    <button class="btn btn-sm btn-success" id="prev_button"><span class=" glyphicon glyphicon-chevron-left"></span> Prev <?=isset($limit)?'10':$limit?></button>
                    <button class="btn btn-sm btn-success" id="next_button">Next <?= isset($limit)?'10':$limit?><span class=" glyphicon glyphicon-chevron-right"></span></button>              </div>
                    
              <input type="hidden" value="<?= isset($limit)?'10':$limit?>" id="field_limit"/>
              <input type="hidden" value="<?= isset($offset)?'0':$offset?>" id="field_offset"/>
              <input type="hidden" value="" id="field_order"/>
              <?php
			  if(isset($total_numrows)){?>
				<input type="hidden" id="total_numrows" value="<?=$total_numrows?>"/>			  
			  <?php }
			  ?>
              </div>
            </div>
      </div>
    </div>

</div>