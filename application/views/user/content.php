    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <p><label class="label label-info">Please Fill up all the Fields</label></p>
   
			<form class="form-horizontal" role="form" id='registerform' action="<?= base_url(); ?>user/register" method="post">
                <div class="form-group">
                    <label class="control-label col-sm-2 label label-info" for="user_login">Login ID</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="user_login" name="user_login"
                            placeholder="Enter User ID" minlength="3" maxlength="10" required>
                        <span id="msg_id_used" class="label label-danger error" style="display:none;">ID Already Used</span>
                    </div>
                </div>            
                <div class="form-group">
                    <label class="control-label col-sm-2 label label-info" for="user_password">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="user_password" name="user_password"
                            placeholder="Enter Password" maxlength="100" minlength="10" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2 label label-info" for="user_password_c">Confirm Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="user_password_c" name="user_password_c"
                            placeholder="Confirm Password" maxlength="100" minlength="10">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2 label label-info" for="user_fullname">Full Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="user_fullname" name="user_fullname"
                            placeholder="Enter your Full Name">
                    </div>
                </div>                
                                                                                          
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10 text-right">
                        <button type="submit" class="btn btn-default">Submit</button>
                        <button type="button" class="btn btn-default" id="go_back">Go Back</button>
                    </div>                    
                </div>
            </form>
            </div>
        </div>
    </div>

    <hr>