<div class="container">
	<div class="row text-center jumbotron">
    	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        	<?php if($res == -1){?>
	        	<label class="label label-danger"><?= $msg; ?></label>
            <?php }else{ ?>
            	<label class="label label-success"><?= $msg; ?></label>
            <?php } ?>
			<hr class="small">
            <?php if($res == -1){?>
	        	<button class="btn btn-primary" id="goback">Go Back</button>
            <?php }else{ ?>
   	        	<button class="btn btn-primary" id="continue">Continue</button>
            <?php } ?>
        </div>
    </div>
</div>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <ul class="list-inline text-center">
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-github fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="copyright text-muted">Copyright &copy; <a href="mailto:guilleglad@hotmail.com">guilleglad@hotmail.com</a> 2016</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="<?= base_url(); ?>resources/js/jquery.js"></script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url(); ?>resources/js/bootstrap.min.js"></script>
	<!-- After Register -->
    <script src="<?= base_url(); ?>resources/js/after_register_user.js"></script>
</body>

</html>