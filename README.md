**NearSource Test MVC**

**ESPAÑOL**
  El Framework seleccionado fue Codeigniter con base de datos MySQL. Mi idea fue crear la aplicación al estilo móvil, es decir, que se pudiera ver como una aplicación móvil desde cualquier dispositivo y que se adaptara a las pantallas de hoy en día, por ello la presentación de los registros y la mayoría de la parte visual podrá ser vista muy diferente a lo que normalmente se presenta en sistemas de administración de usuarios.

1. Se debe ejecutar el script de MySQL llamado prj_ns.sql, este script creará la base de datos prj_ns y dos tablas llamadas user y employee, con un usuario admin con password admin y 20 registros en la tabla employee.
2. Se debe crear un sitio en el Servidor web con nombre nsp, dentro del sitio se debe pegar todo el contenido del archivo ZIP suministrado.
3. Se debe asegurar que el módulo rewrite_mod esté activado en el servidor Apache.
4. Modificar el archivo config.php de la carpeta application->database y establecer las propiedades hostname,username y password en relación al servidor de base de datos.

Después de esto se debe poder iniciar el sitio en http://{servidor}/nsp

**Todo el desarrollo se puede descargar desde https://bitbucket.org/guilleglad/nsp/src**

**ENGLISH**
CodeIgniter was the framework chosen and MySQL. My idea was to develope a web mobile styled application, I wanted it were possible to be seen from any device, and be responsive enough to be seen on any screen, that’s why the record list presentation and most of the visuals, could be seen very different of what people is used to see on an user administration system.

1.	Run the MySQL script named prj_ns.sql, the script wil create the database prj_ns and the tables user and employee, including user admin with password admin, and 20 records in table employee.
2.	Create a site named nsp and copy all the content of the ZIP file inside this folder.
3.	Be sure rewrite_mod module is activated in apache server.
4.	Modify config.php under application->database, and set the specific properties hostname, username and password based on the database server.

Now the site should be available on http://{server}/nsp

**The Source Code can be pulled from https://bitbucket.org/guilleglad/nsp/src**

URL: http://nsp.net23.net/