-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-05-2016 a las 09:59:13
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `prj_ns`
--
CREATE DATABASE IF NOT EXISTS `prj_ns` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `prj_ns`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `admin_password` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `admin_fullname` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_password`, `admin_fullname`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `employee_id` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `employee_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `employee_address` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `employee_email` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `employee_phonenumber` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `employee_contractdate` date NOT NULL,
  `employee_birthdate` date DEFAULT NULL,
  `employee_freelance` tinyint(4) NOT NULL,
  `employee_hourlyfee` decimal(10,2) NOT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `employee`
--

INSERT INTO `employee` (`employee_id`, `employee_name`, `employee_address`, `employee_email`, `employee_phonenumber`, `employee_contractdate`, `employee_birthdate`, `employee_freelance`, `employee_hourlyfee`) VALUES
('V-12345', 'test name test', 'test', 'test@gmail.com', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-14348908', 'Guillermo Garcia', 'Tariba', 'guilleglad@hotmail.com', '0412-7927616', '2016-05-15', '2016-05-15', 1, '10.00'),
('V-17677286', 'Belkis Xiomara', 'Tucape', 'belxio15@gmail.com', '', '2016-05-15', '1985-03-27', 0, '0.00'),
('V-4210235', 'Carmen Garcia', 'Tariba', 'carmencgarciag@hotmail.com', '0276-3946216', '2016-05-15', '1953-11-05', 0, '0.00'),
('V-test', 'test name test', 'test address', 'test@gmail.com', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test10', 'test name 10', 'test address 10', 'test10@gmail.com', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test100', 'test100 name', 'test address 100', 'test100@gmail.com', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test1000', 'test name 1000', 'test address 1000', '', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test11', 'test name 11', 'test address\r\n', '', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test12', 'test name 12', 'test address 12', '', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test13', 'test name 13', 'test address 13', '', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test15', 'testname15', 'testaddress15', '', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test16', 'test name 16', 'test address 16', '', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test20', 'test name 20', 'test address 20', '', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test21', 'test name 21', 'test address 21', 'test21@gmail.com', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test22', 'test name 22', 'test address 22', '', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test23', 'test name 23', 'test address 23\r\n', '', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test25', 'test name 25', 'test address 25', '', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-test3', 'test name 3', 'test address 3', 'test@test.com', '', '2016-05-16', '2016-05-16', 0, '0.00'),
('V-testt', 'test name testt', 'test address\r\n\r\n', '', '', '2016-05-16', '2016-05-16', 0, '0.00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
