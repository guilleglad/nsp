// JavaScript Document
$(document).ready(function(e) {
	//LOGIN AJAX FUNCTION 
	$("#form_login").submit(function(event){
		event.preventDefault();
		$.ajax({
			url:$(this).attr("action"),
			type: $(this).attr("method"),
			async:false,
			data: $(this).serialize(),
			}).done(function(data){
				if(data == 'OK')
					window.location.replace("/nsp/main/index");
				else
					alert(data);
				});
		});
	
	//LINKS 
	$("#login_link").click(function(){
		$("#form_login")[0].reset();
		});
		
	$("#logout_link").click(function(event){
		event.preventDefault();
		$.ajax({
			url:$(this).attr("data-target"),
			}).done(function(){
				window.location.replace("/nsp/main/index");
			});
		});	
	$("#signup_btn").click(function(){
		window.location.replace("/nsp/user/signup");
		});
		


});


