// JavaScript Document
$(document).ready(function(e) {
	function check_prev_button(){
		if($("#field_offset").val() == '0')
			$("#prev_button").hide();
		else
			$("#prev_button").show();
	}
	function check_next_button(){
		var limit = eval($("#field_limit").val());
		var offset = eval($("#field_offset").val());
		var pos = eval($("#field_pos").val());
		if(limit+offset >= $("#total_numrows").val())
			$("#next_button").hide();
		else
			$("#next_button").show();
	}
	check_prev_button();
	check_next_button();
	$("#next_button").click(function(event){
		event.preventDefault();
		var where = $("#mix_filter").val();
		var date1 = $("#date1").val();
		var date2 = $("#date2").val();
		var limit = eval($("#field_limit").val());
		var offset = eval($("#field_offset").val());
		if($("#field_order").val() == '')
		{
		var postdata = {'limit': limit,'offset': offset+limit,'where':where,'date1':date1,'date2':date2}; 
		$.ajax({
			url:"/nsp/employee/index_ajax_where",
			type: "POST",
			data: postdata,
			dataType:"text",
			beforeSend: function(){
				$(".loader").show();
				},
			}).done(function(res){
				$("#table_body").html(res);		
				hide_chevrons();	
				$("#field_order").val('');					
				$("#field_offset").val(offset+limit);
				check_prev_button();
				check_next_button();
				assign_delete();						
				$(".loader").hide();
			});
		}else{
			var postdata = {'limit': limit,'offset': offset+limit,'where':where,'orderby':$("#field_order").val(),'date1':date1,'date2':date2}; 
		$.ajax({
			url:"/nsp/employee/index_orderby",
			type: "POST",
			data: postdata,
			dataType:"text",
			beforeSend: function(){
				$(".loader").show();
				},
			}).done(function(res){
				$("#table_body").html(res);	
				$(".loader").hide();
				$("#field_offset").val(offset+limit);
				check_prev_button();
				check_next_button();
				assign_delete();								
			});			
		}

	});
	$("#prev_button").click(function(event){
		event.preventDefault();
		var where = $("#mix_filter").val();
		var date1 = $("#date1").val();
		var date2 = $("#date2").val();
		var limit = eval($("#field_limit").val());
		var offset = eval($("#field_offset").val());	
		if($("#field_order").val() == '')
		{
		var postdata = {'limit': limit,'offset': offset-limit,'where':where,'date1':date1,'date2':date2}; 
		$.ajax({
			url:"/nsp/employee/index_ajax_where",
			type: "POST",
			data: postdata,
			dataType:"text",
			beforeSend: function(){
				$(".loader").show();
				},
			}).done(function(res){
				$("#table_body").html(res);		
				hide_chevrons();	
				$("#field_order").val('');					
				$("#field_offset").val(offset-limit);
				check_prev_button();
				check_next_button();
				assign_delete();				
				$(".loader").hide();
			});
		}else{
			var postdata = {'limit': limit,'offset': offset-limit,'where':where,'orderby':$("#field_order").val(),'date1':date1,'date2':date2}; 
		$.ajax({
			url:"/nsp/employee/index_orderby",
			type: "POST",
			data: postdata,
			dataType:"text",
			beforeSend: function(){
				$(".loader").show();
				},
			}).done(function(res){
				$("#table_body").html(res);	
				$(".loader").hide();
				$("#field_offset").val(offset-limit);
				check_prev_button();
				check_next_button();
				assign_delete();								
			});		
		}
	});	
	$("#logout_link").click(function(event){
		event.preventDefault();
		$.ajax({
			url:$(this).attr("data-target"),
			}).done(function(){
				window.location.replace("/nsp/main/index");
			});
		});	
    $("#filter_btn").click(function(event){
		event.preventDefault();
		var where = $("#mix_filter").val();
		var date1 = $("#date1").val();
		var date2 = $("#date2").val();
		var limit = eval($("#field_limit").val());
		var offset = eval($("#field_offset").val());			
		var postdata = {'limit': limit,'offset': offset,'where':where,'date1':date1,'date2':date2}; 
		$.ajax({
			url:"/nsp/employee/index_ajax_where",
			type: "POST",
			data: postdata,
			dataType:"text",
			beforeSend: function(){
				$(".loader").show();
				},
			}).done(function(res){
				$("#table_body").html(res);		
				hide_chevrons();	
				$("#field_order").val('');					
				$(".loader").hide();
				check_prev_button();
				check_next_button();	
				assign_delete();							
			});
		});
		
	var function_order = function(event){
		var obj = $(this);
		event.preventDefault();
		var where = $("#mix_filter").val();
		var date1 = $("#date1").val();
		var date2 = $("#date2").val();		
		var limit = eval($("#field_limit").val());
		var offset = eval($("#field_offset").val());			
	var postdata = {'limit': limit,'offset': offset,'where':where,'orderby':obj.attr('data-value'),'date1':date1,'date2':date2}; 
		$.ajax({
			url:"/nsp/employee/index_orderby",
			type: "POST",
			data: postdata,
			dataType:"text",
			beforeSend: function(){
				$(".loader").show();
				},
			}).done(function(res){
				$("#table_body").html(res);	
				show_chevron(obj,false);
				$("#field_order").val(obj.attr('data-value'));
				$(".loader").hide();
				check_prev_button();
				check_next_button();	
				assign_delete();				
			});
		};
		
	$("#order_id").click(function_order);	
	$("#order_name").click(function_order);	
	$("#order_email").click(function_order);	
	
    $( "#date1" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      onClose: function( selectedDate ) {
        $( "#date2" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#date2" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      onClose: function( selectedDate ) {
        $( "#date1" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
	function hide_chevrons()
	{
		$(".chevron_header").addClass("hidden");
	}
	function show_chevron(obj,is_text)
	{
		var chevron;
		var x;
		if(is_text)
			x = obj;
		else
			x = obj.attr('data-value');
		
		switch(x){
			case "employee_id":
				chevron = $("#id_chevron");
				break;
			case "employee_name":
				chevron = $("#name_chevron");
				break;
			case "employee_email":
				chevron = $("#email_chevron");
				break;
		}
		hide_chevrons();
		
		if(chevron.hasClass("hidden"))
			chevron.removeClass("hidden");
		if(chevron.hasClass("glyphicon-chevron-up"))
		{
			chevron.removeClass("glyphicon-chevron-up");
			chevron.addClass("glyphicon-chevron-down");						
		}else{
			chevron.removeClass("glyphicon-chevron-down");
			chevron.addClass("glyphicon-chevron-up");						
		}
	}
	$("#filter_close_btn").click(function(event){
		event.preventDefault();
		$("#filter_row").slideUp();
		$("#row_show_filter").removeClass("hidden");
		});
	$("#row_show_filter").click(function(event){
		event.preventDefault();
		$("#filter_row").slideDown();
		$("#row_show_filter").addClass("hidden");
		});
	$("#filter_row").hide();
	
	function assign_delete(){
		$(".mydelete").click(function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		if(confirm("You are about to remove this Record. Are you sure?")){
			$.ajax({
				url:$(this).attr("href"),
				type:"GET",
				dataType:"text"
			}).done(function(data){
				if(data == '1')
					alert("Record Deleted");
				else
					alert("The Record couldn't be removed");
				window.location.replace("employee");
			});
		}
	});		
	}
	assign_delete();
});

