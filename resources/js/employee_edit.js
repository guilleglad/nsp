// JavaScript Document
$(document).ready(function(e) {
	$("#div_employee_hourlyfee").hide();
	if($("#employee_freelancer").is(":checked"))
		$("#div_employee_hourlyfee").show();

    $("#employee_freelancer").change(function(event){
		$("#div_employee_hourlyfee").toggle($("#employee_freelancer").val());
		$("#employee_hourlyfee").val('');
		});
	$("#employee_contractdate").datepicker({
		dateFormat: "dd/mm/yy"
	});
	$("#employee_birthday").datepicker({
		dateFormat: "dd/mm/yy"
	});
	
	$("#go_back_employee").click(function(){
		window.location.replace('/nsp/employee');
	});
	
	$("#employee_id").focusout(function(e) {
		if(!$("#employee_id-error").is(":visible")){
		$.ajax({
			url:'check_id',
			type: 'POST',
			async:true,
			data: {'employee_id':$("#employee_id").val()},
			dataType:"text",
			success: function(){
				$("#loader").addClass('hidden');
			},
			beforeSend: function(){
				$("#loader").removeClass('hidden');
			},
			}).done(function(data){
				if(data == '1')
				{
					$("#employee_id").select();
					$("#msg_id_used").show();
				}
				else{
					$("#msg_id_used").hide();
				}
			});
		}
    });
	
	$('#registerform').validate({
    rules:{
		'employee_id':{
			required: true,
			minlength: 5,
			pattern: '^(V-)([a-zA-Z0-9])+',
		},
		'employee_name':{
			required: true,
			minlength: 10,
		},
		'employee_address':{
			required: true,
		},	
		'employee_phonenumber':{
			required: false,
			pattern: '^([0-9]{4})-([0-9]{7})',
		},
		'employee_contractdate':{
			required: true,
		},				
		'employee_birthday':{
			required: true,
		},			
		'employee_hourlyfee':{
			required: $("#employee_freelancer").val()?true:false,
		}
    },
    messages:{
		'employee_id':{
			required:"Field ID Required",
			minlength:"Your ID must be at least {0} character long",
			pattern:"You must match the Pattern, starting with V-",
		},		
		'employee_name':{
			required:"Field Name Required",
			minlength:"Your Name must be at least {0} character long"
		},
		'employee_address':{
			required:"Field Address Required",
		},		
		'employee_phonenumber':{
			pattern:"Invalid Format Ex.(1234-1234567)",
		},
		'employee_contractdate':{
			required:"Field Contract Date Required",
		},						
		'employee_birthday':{
			required:"Field Birthday Date Required",
		},		
		'employee_hourlyfee':{
			required:"Field Hourly Fee is Required if you are a Freelancer",
		},										
    }
	});
});