// JavaScript Document
$(document).ready(function(e) {
	$("#go_back").click(function(){
	window.location.replace("/nsp");
	});
    $('#registerform').validate({
    rules:{
		'user_id':{
			required: true,
			minlength: 5
		},
        'user_password':{
            required: true,
            minlength: 5
        },
        'user_password_c':{
            required: true,
            minlength: 5,
            equalTo: "#user_password"
        },
		'user_fullname':{
			required: true,
			minlenght: 5
		}		
    },
    messages:{
		'user_id':{
			required:"This field is required",
			minlength:"Your name must be at least 5 character long"
		},
        'user_password':{
            required:"This field is required",
            minlength:"Your password must be at least 5 character long"
        },
		'user_fullname':{
			required:"This field is required",
			minlength:"Your login must be at least 5 character long"
		},			
        'user_password_c':{
            required:"This field is required",
            minlength:"Your password must be at least 5 character long",
            equalTo:"Your password does not match"
        }
    }
	});
});